Testimonials = new Mongo.Collection("testimonials");
Testimonials.allow({
  insert: function(){
    return false;
  },
  update: function(){
    return false;
  },
  remove: function(){
    return false;
  }
});

Announcement = new Mongo.Collection("announcement");
Announcement.allow({
  insert: function(){
    return false;
  },
  update: function(){
    return false;
  },
  remove: function(){
    return false;
  }
});


Queries = new Mongo.Collection("Queries");
Queries.allow({
  insert: function(){
    return false;
  },
  update: function(){
    return false;
  },
  remove: function(){
    return false;
  }
});
