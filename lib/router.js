FlowRouter.route('/', {
  action: function(params) {
    BlazeLayout.render("layout",{ main: "info"});
    
    Meteor.setTimeout(function () {
      $('.navbar').removeClass('navbar-scroll')
    }, 500);
    
  },
  name:'home'
});

FlowRouter.route('/course/:slug', {
  action: function(params) {
    BlazeLayout.render("layout",{ main: "courseInfo"});
  },
  name: 'courseInfo'
});

FlowRouter.route('/linux-admin-and-scripting', {
  action: function(params) {
    BlazeLayout.render("layout",{ main: "multipleCourses"});
  },
  name: 'multipleCourses'
});

FlowRouter.route('/courses', {
  action: function(params) {
    BlazeLayout.render("layout",{ main: "AllCourses"});
  },
  name: 'AllCourses'
});

FlowRouter.route('/trainers', {
  action: function(params) {
    BlazeLayout.render("layout",{ main: "Trainers"});
  },
  name: 'Trainers'
});

FlowRouter.route('/login', {
  action: function(params) {
    BlazeLayout.render("layout",{ main: "Login"});
  },
  name:'logIn'
});

FlowRouter.route('/profile', {
  action: function(params) {

    if(!Meteor.user() && !Meteor.loggingIn()){
      FlowRouter.go("/");
    }else{
      Meteor.subscribe("currentUserTestimonial");
      BlazeLayout.render("layout",{ main: "profile"});
    }
  },
  name:'logIn'
});

FlowRouter.route('/contact', {
  action: function(params) {
    BlazeLayout.render("layout",{ main: "contact"});
  },
  name: 'contact'
});


FlowRouter.route('/reset-password/:token',{
  action: function(params) {
    BlazeLayout.render("layout",{ main: "resetPassword"});
  },
  name: 'resetPassword'
});

scrollToTop = function () {
  if(Meteor.isClient){
    var scrollTo = window.currentScroll || 0;
    $('body').scrollTop(scrollTo);
    $('body').css("min-height", 0);
  }
}