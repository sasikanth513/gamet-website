Meteor.methods({
  sendEmail: function (fullName, email, mobileNumber, message, course) {
    var msg = "User Details.<br>"+
    "Full Name: "+fullName+"<br>"+
    "Email: "+email+"<br>"+
    "Mobile: "+mobileNumber+"<br>"+
    "Message: "+message+"<br>"+
    "Course: "+course+"<br>";

    let now = new Date();

    let obj = {
      name: fullName,
      email: email,
      mobile: mobileNumber,
      message: message,
      course: course,
      createdAt: now
    };

    if(this.userId){
      _.extend(obj, { userId: this.userId});
    }
    Queries.insert(obj)
    Email.send({
      to: 'gamut.trainings@gmail.com',
      // to: 'sasi.kanth80@gmail.com',
      from: 'no-reply@gamutgurus.com',
      subject: "Message from website",
      html: msg
    });
    return true;
  },
  sendTrainerEmail: function (obj) {
    var msg = "Trainer Details.<br>"+
    "Full Name: "+obj.firstName +" "+obj.lastName+"<br>"+
    "Email: "+obj.email+"<br>"+
    "Mobile: "+obj.mobileNumber+"<br>"+
    "Course: "+obj.courseName+"<br>"+
    "About Course: "+obj.aboutCourse+"<br>"+
    "About Yourself: "+obj.aboutYourSelf+"<br>"

    
    Email.send({
      to: 'gamut.trainings@gmail.com',
      from: 'no-reply@gamutgurus.com',
      subject: "Message from website",
      html: msg
    });
    return true;
  },
  saveTestimonial(text, testimonialCourse, rating){
    var res = Testimonials.findOne({ userId: this.userId});
    if(!this.userId){
      throw new Meteor.Error(404, "Please login to create testimonial.");
      return;
    }
    var userName = Meteor.users.findOne({ _id: this.userId}).profile.name;
    if(res){
      Testimonials.update({ userId: this.userId}, { $set: { text: text, course: testimonialCourse, approved: false, rating: rating } });
    }else{
      Testimonials.insert({ userId: this.userId, text: text, name: userName, createdAt: new Date(), course: testimonialCourse, approved: false, rating: rating});
    }
    return true;
  },
  updateTestimonial(text, testimonialCourse, rating){
    Testimonials.update({ userId: this.userId}, { $set: { text: text, course: testimonialCourse,approved: false, rating: rating } });
    return true;
  },
  saveAnnouncement: function(text){
    var res = Announcement.findOne({});
    if(res){
      Announcement.update({ _id: res._id}, { $set: { text: text } });

    }else{
      Announcement.insert({ text: text, createdAt: new Date()})
    }
    return true;
  },
  sendResetEmail: function(email){
    var res = Accounts.findUserByEmail(email);
    if(res){
      Accounts.sendResetPasswordEmail(res._id);
      return true;
    }else{
      throw new Meteor.Error(403,"Email not found");
      return;
    }
    return true;
  },
  updateTestAdmin(approved, position, tid) {
    Testimonials.update(tid, { $set:  {
        approved: approved,
        position: Number(position),
        positionUpdate: new Date()
      }
    });
    return true;
  }
});
