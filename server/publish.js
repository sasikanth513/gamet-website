Meteor.publish("currentUserTestimonial", function(){
  if(this.userId){
    return Testimonials.find({ userId: this.userId});
  }
  this.ready();
});

Meteor.publish("testimonials", function(limit){
  return Testimonials.find({approved: true}, { sort: { createdAt: -1}, limit: limit});
});

Meteor.publish(null, function(argument){
  return Announcement.find({});
});

Meteor.publish('adminQueries', function () {
  return Queries.find({});
});

Meteor.publish('testimonialCount', function() {
  Counts.publish(this, 'testimonialCount', Testimonials.find());
});

Meteor.publish('adminTestimonials', function () {
  return Testimonials.find({});
});
