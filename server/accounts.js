Accounts.emailTemplates.siteName = "GAMUT Technologies";
Accounts.emailTemplates.from = "GAMUT Technologies Team <no-reply@gamutgurus.com>";

Accounts.emailTemplates.resetPassword.subject = function (user) {
    return "Reset password";
};

Accounts.urls.resetPassword = function (token) {
  return Meteor.absoluteUrl('reset-password/' + token);
};

// Accounts.urls.verifyEmail = function (token) {
//   return Meteor.absoluteUrl('verify-email/' + token);
// };