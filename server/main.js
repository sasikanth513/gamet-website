import { Meteor } from 'meteor/meteor';

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

Meteor.startup(() => {
  // code to run on server at startup
  if (Testimonials.find({}).count() < 50) {
    for (let i = 0; i < 50; i += 1) {
      const userId = '5HuzSSc3qiBovGraK';
      const text = Fake.paragraph(getRandomInt(5, 10));
      Testimonials.insert({ userId: userId, text: text, name: Fake.word(), createdAt: new Date(), course: "DevOps", approved: false, rating: getRandomInt(1, 5)});
    }
  }
});
