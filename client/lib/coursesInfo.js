// puppet banner not found, "ANT,MAVEN,GRADLE", shell-scripting
CoursesInfo = [];

var devops = {
  url: "devops",
  title: 'DEVOPS',
  duration: '(70 hours/45 days <br> Extensive, Hands-on & Command-line Training)',
  secondPageDesc: '(70 hours/45 days Extensive, Hands-on & Command-line Training)',
  newBatch: 'Aug 21, 2016',
  price: '22,000',
  // originalPrice: '20,000',
  courseHighlights: [
    "Trainer P.Nageswara Rao has 12+ years of vast experience in DevOps related technologies. He has worked on large scale applications and world's largest ecommerce orgnaizations.",
    "Gamut Gurus is a pure and dedicated DevOps Training portal. So, you will get laser focused and quality training to the core.",
    "Command line based training without any slides.",
    "Trainer has completed more than 150 batches and 600+ students are placed.",
    "Placement assistance and after job support.",
    "We will be running more than 4 batches a day. So, In case you miss some classes, you can take the clases in any batch."
  ],
  // headerImage: '/images/Banner/DevOps_bnr.jpg',
  // thumbnail: '/images/Banner/DevOps_bnr.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Mar 03, 2017",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Mar 03, 2017",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Mar 03, 2017",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Mar 03, 2017",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to DevOps and its Necessities",
      description:"-  What is DevOps, Why DevOps, DevOps Trend\
      <br>- Views of DevOps, Influences and Origins, development and Operations Conflicts\
      <br>- DevOps and Tools Stack\
      <br>- Agile Vs DevOps\
      <br>- Roles and Responsibilities of DevOps Engineer\
      <br>- Typical day of a DevOps Engineer"
    },
    {
      header: "2. Basics of DevOps",
      description:"- Code Delivery Process: Software develpment, Testing, Maintanence and Operations\
      <br>- Build and Deployment Process\
      <br>- Environment management: QA, UAT, STAGE, PROD Environments\
      <br>- Build and Deployment Best Practices\
      <br>- Infrastructure Automation and scalability"
    },
    {
      header: "3. GIT - Version Control Systems",
      description:"- What is Version controling, Why We Need Version Control Systems\
      <br>- GIT Vs Other Version control systems (SVN, Perforce, TFS..etc.)"
    },
    {
      header: "4. Maven - Build Tool",
      description:"-  Build and Deployment Basics: Compilation, Packaging, Testing\
      <br>"
    },
    {
      header: "5. Jenkins - Continuous Integration, Delivery and Deployment (CI/CD)",
      description:"-  What is Continuous Integration, Delivery and Deployment\
      <br>"
    },
    {
      header: "6. Docker - Containerization",
      description:"-  Virtualization Vs Containerization\
      <br>"
    },
    {
      header: "7. Chef - Configuration management",
      description:"-  Necessities of Configuration management tool\
      <br>"
    },
    {
      header: "8. AWS - Cloud Platform",
      description:"- AWS Services\
      <br>"
    },
    {
      header: "9. Ruby - Scripting",
      description:"- Ruby Basics\
      <br>"
    },
    {
      header: "10. Linux Advanced",
      description:"- Linux Administration Concepts\
      <br>"
    },
    {
      header: "11. Miscellenious",
      description:"Monitoring, Tomcat, Logging\
      <br>"
    },
  ],
  certification: " Gamut Gurus has a colaboration with Proxytem Software Services Pvt. Ltd. who is our training and certification partner.\
  At the end of your course, you will get a course completion and DevOps\
  Certified Engineer Certification from us.",
  reimbersement: "If your company is providing corporate reimbersment, You will get a quotation and we can work with your company HR/Manager to make your reimbersment process go smooth.",
  trainer: "Trainer P.Nageswara Rao has 12+ years of vast experience in DevOps related technologies. He has worked on large scale applications and world's largest ecommerce orgnaizations.\
  Trainer has completed more than 150 batches and 600+ students are placed."
}

CoursesInfo.push(devops);

var buildAndRelease = {
  url: "build-and-release",
  title: 'BUILD AND RELEASE',
  duration: '(70 hours/45 days - Extensive Command-line oriented Training)',
  newBatch: 'Aug 21, 2016',
  price: '20,000',
  // originalPrice: '20,000',
  courseHighlights: [
    "70+ Hours Extensive Training",
    "For IT job seekers, Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  // headerImage: '/images/Banner/BuildnRel.jpg',
  thumbnail: '/images/Banner/BuildnRel.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Environment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Ideal Software develpment, Quality Analysis, Build & Release process",
      description:"<b>Learning Objectives -</b>In this module, you willl learn general Software development, Build and release process for different code bases and all tools. <br><b>Topics -</b> Software Development Life Cycle (SDLC), Software Development models, Quality analysis, Operations. Co-ordination with stakeholders to move the code from developent phase to release, Web Applications Vs Standalone applications, Packaging specifics of Java, .Net, C, C++ codebase builds."
    },
     {
      header: "3. Linux/Unix commands, Concepts, Tools Installation and Setup ",
      description:"<b>Learning Objectives -</b>In this module you will learn all Linux commands, concepts, installation and setup of various tools   <br><b>Topics -</b>~50+ Linux commands, General concept of Software installations on Linux/Unix, rpm, bin, zip, tar.gz, zip..etc., Linux Shell, Environment variables, PATH, Bashfile, Linux process hirarchy, Installation of Java, ANT, Maven, SVN, Git, Jenkins, Apache Tomcat..etc."
    },

    {
      header: "4. ANT Build Automation Tool",
      description:"<b>Learning Objectives -</b>In this module, you will learn the core concepts of ANT build automation tool starting from very basics to advanced. After completion of this module, you will be able to write build files to automate any build system.<br><b>Topics -</b> Introduction to build tools, manual Vs automation, structure of build.xml, ANT properties, properties file, task, target, target dependency, ~30 freequently used ant tasks, macrodef, contrib ant tasks and Integration, writing custom ant tasks, master build file, building complete project, End-to-End build and Deployment hands-on for a real time Java application,  "
    },

    {
      header: "4. Version Control Systems(VCS) / Source control management Tools:",
      description:"<b>Learning Objectives -</b>In this module, you will learn general concepts of Version control systems so that you can work with any tool.<br><b>Topics -</b> Roll of version control systems in software development and versioning softares, Concept of version controling, Origin, version control systems necessity, features, principles, available version control systems, importance of maintaining the history of software code, change, change management, code overriding, copy merge modify solution, workspace, repository, checkout, Commit, SCM Plan, Integration of ANT and Version control systems"
    },

    {
      header: "4. Subversion VCS:",
      description:"<b>Learning Objectives -</b>In this module you will learn End-to-End workflow of version controling with SVN including administration.<br><b>Topics -</b> prerequisites of Subversion, Installation, creating repository, workspace, "
    },

    {
      header: "5. Gamut of Build and Release Tools set:",
      description: "Build and Deployment Automation: <br>ANT<br>Maven<br><br>Version Control/SCM: <br>Subversion<br>Git<br><br>Continuous Integration:<br>Jenkins/Hudson<br><br>Application/Web Servers:<br>Apache<br>Tomcat<br>Jboss<br><br>Scripting & OS: <br> Shell scripting<br>Redhat Enterprise Linux<br><br>Bug Tracking Tools: <br>Jira<br>Bugzilla<br><br>Quality Analysis: Sonar<br>PMD<br>Cobertura"
    }
  ],
  certification: "GAMUTgurus has collaboration with Proxytem software services Pvt. Ltd. and can help you to get certified in the area of your expertise. Proxytem is one of the leading organizations in certifications and software development.",
  reimbersement: "If you are working proffessional and have reimbersement policy in your company, GAMUTgurus team can help you provide all required invoice and Co-ordinate with your reimbersement team. your company can directly pay the course fee.",
  trainer: "We belive in teaching quality and all our instructors are domain experts having more than 10 years of industry experience."
}

// CoursesInfo.push(buildAndRelease);

var LinuxAdministration = {
  url: "linux-administration",
  title: 'LINUX ADMINISTRATION',
  duration: '(Python<br>Ruby 45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '20,000',
  // originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Linux.jpg',
  thumbnail: '/images/Thumbnail/Redhat.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. System Administration Overview ",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release<br><b>Topics -</b> UNIX, Linux and Open Source, Duties of the System Administrator, Superusers and the Root Login, Sharing Superuser Privileges with Others (su and sudo Commands), TCP/IP Networking Fundamentals, Online Help"
    },
    {
      header: "2. Installation and Configuration",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Planning: Hardware and Software Considerations, Site Planning, Installation Methods and Types, Installation Classes, Partitions, Basic Networking Configuration, Booting to Recovery Mode"
    },
  {
   header: "3. Booting and Shutting Down Linux ",
   description:"Learning Objectives - In this module, you will learn about Automatic Installation of Servers, Continuous Integration, Configuration Deployments and Packaging.\n\nTopics - Boot Sequence, The systemd Daemon, The systemctl Command, Targets vs. Run Levels"
  },
  {
   header: "4. Installation and Configuration ",
   description: "Learning Objectives - In this module, you will learn about Performance aspects of the Infrastructure from an Enterprise perspective and Implementation of Security to make environment more secure.\n\nTopics - Operating System tuning concepts and it's Concerns, Types of Disk Schedulers, Performance and Use Cases, Network tuning Parameters and their Influence, Understand the Security at the OS and Network level, Configure Linux Firewall and other security aspects for a secured environment."
  }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

// CoursesInfo.push(LinuxAdministration);

var Chef = {
  url: "chef",
  title: 'CHEF Infrastructure Automation',
  duration: '(45 hours - 30 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Chef.jpg',
  thumbnail: '/images/Thumbnail/Chef.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Mar 03, 2017",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Mar 03, 2017",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Mar 03, 2017",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Mar 03, 2017",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    },
  {
   header: "3. Implement Automated Installations and Deployments",
   description:"Learning Objectives - In this module, you will learn about Automatic Installation of Servers, Continuous Integration, Configuration Deployments and Packaging.\n\nTopics - Installation of Linux Servers using PXE boot or kick start method, Yum repository setup and Automatic system updates. Configuration of SVN and GIT."
  },
  {
   header: "4. Understand Performance tuning aspects and basic Security for Infrastructure",
   description: "Learning Objectives - In this module, you will learn about Performance aspects of the Infrastructure from an Enterprise perspective and Implementation of Security to make environment more secure.\n\nTopics - Operating System tuning concepts and it's Concerns, Types of Disk Schedulers, Performance and Use Cases, Network tuning Parameters and their Influence, Understand the Security at the OS and Network level, Configure Linux Firewall and other security aspects for a secured environment."
  },
  {
    header: "5. Installation & Configuration of Jenkins and Puppet",
    description: "Learning Objectives - In this module, you will learn about Installation & Configuration of Jenkins and Puppet.\n\nTopics - Installation of Jenkins, Authentication with LDAP, UNIX etc, Integration with SVN, Remote command execution, Puppet Installation and Configuration, Puppet manifests and examples, Puppet with SVN."
  },
  {
    header: "6. Introduction to Automation with Ansible and SaltStack",
    description: "Learning Objectives - In this module, you will learn about the basics of Ansible, Ansible Playbooks, Ansible Inventory/Dynamic Inventory, Ansible Patterns.\n\nTopics - Infrastructure as Code, Ansible Installation, Ansible Communication framework, Ansible Playbooks, Ansible Inventory/Dynamic Inventory, Ansible Patterns, Sample Scripts, SALTStack States, SLS and Top files, Namespaces, Renderers, Templating Modules, Orchestration."
  },
  {
    header: "7. Automation with Chef",
    description: "Learning Objectives - In this module, you will learn about the Basics of Chef, Chef Cookbooks, Chef Architecture, Tools - Knife & Scripting and Chef Development Kit.\n\nTopics - Chef Recipes, Chef Cookbooks, Chef Architecture, Tools - Knife & Scripting, Chef Development Kit (ChefDK)"
  },
  {
    header: "8. Monitoring, Logging, Tomcat and System Tools",
    description: "Learning Objectives - In this module, we will look at Monitoring, logging and auditing and also various DevOps tools/commands that are necessary for the day-to-day activities. We will also look at setting up Tomcat Server.\n\nTopics - Introduction to various logging tools, Understand System auditing, Install and Configure Nagios Monitoring for the Infrastructure, Installation of Tomcat Server and examples, Understand Openssl and Openssh details, Understand rsync for backups, Understand Commands like: lsof, netstat, Understand Virtual Memory, Free, top, vmstat, iostat, uptime, find, screen, strace, Disk commands like - df, du, mkfs, tune2fs, fdisk, dd, Understand /etc/fstab, Mount commands."
  }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(Chef);

var Puppet = {
  url: "puppet",
  title: 'PUPPET Infrastructure Automation',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Puppet.jpg',
  thumbnail: '/images/Thumbnail/Puppet.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    },
  {
   header: "3. Implement Automated Installations and Deployments",
   description:"Learning Objectives - In this module, you will learn about Automatic Installation of Servers, Continuous Integration, Configuration Deployments and Packaging.\n\nTopics - Installation of Linux Servers using PXE boot or kick start method, Yum repository setup and Automatic system updates. Configuration of SVN and GIT."
  },
  {
   header: "4. Understand Performance tuning aspects and basic Security for Infrastructure",
   description: "Learning Objectives - In this module, you will learn about Performance aspects of the Infrastructure from an Enterprise perspective and Implementation of Security to make environment more secure.\n\nTopics - Operating System tuning concepts and it's Concerns, Types of Disk Schedulers, Performance and Use Cases, Network tuning Parameters and their Influence, Understand the Security at the OS and Network level, Configure Linux Firewall and other security aspects for a secured environment."
  },
  {
    header: "5. Installation & Configuration of Jenkins and Puppet",
    description: "Learning Objectives - In this module, you will learn about Installation & Configuration of Jenkins and Puppet.\n\nTopics - Installation of Jenkins, Authentication with LDAP, UNIX etc, Integration with SVN, Remote command execution, Puppet Installation and Configuration, Puppet manifests and examples, Puppet with SVN."
  },
  {
    header: "6. Introduction to Automation with Ansible and SaltStack",
    description: "Learning Objectives - In this module, you will learn about the basics of Ansible, Ansible Playbooks, Ansible Inventory/Dynamic Inventory, Ansible Patterns.\n\nTopics - Infrastructure as Code, Ansible Installation, Ansible Communication framework, Ansible Playbooks, Ansible Inventory/Dynamic Inventory, Ansible Patterns, Sample Scripts, SALTStack States, SLS and Top files, Namespaces, Renderers, Templating Modules, Orchestration."
  },
  {
    header: "7. Automation with Chef",
    description: "Learning Objectives - In this module, you will learn about the Basics of Chef, Chef Cookbooks, Chef Architecture, Tools - Knife & Scripting and Chef Development Kit.\n\nTopics - Chef Recipes, Chef Cookbooks, Chef Architecture, Tools - Knife & Scripting, Chef Development Kit (ChefDK)"
  },
  {
    header: "8. Monitoring, Logging, Tomcat and System Tools",
    description: "Learning Objectives - In this module, we will look at Monitoring, logging and auditing and also various DevOps tools/commands that are necessary for the day-to-day activities. We will also look at setting up Tomcat Server.\n\nTopics - Introduction to various logging tools, Understand System auditing, Install and Configure Nagios Monitoring for the Infrastructure, Installation of Tomcat Server and examples, Understand Openssl and Openssh details, Understand rsync for backups, Understand Commands like: lsof, netstat, Understand Virtual Memory, Free, top, vmstat, iostat, uptime, find, screen, strace, Disk commands like - df, du, mkfs, tune2fs, fdisk, dd, Understand /etc/fstab, Mount commands."
  }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(Puppet);

var ansible = {
  url: "ansible",
  title: 'Ansible',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Ansible.jpg',
  thumbnail: '/images/Thumbnail/Ansible.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(ansible);

var aws = {
  url: "aws",
  title: 'Amazon Web Services',
  aliases: ['aws'],
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Amazon.jpg',
  thumbnail: '/images/Thumbnail/Amazon.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(aws);

var azure = {
  url: "azure",
  title: 'Azure Windos Cloud System',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/WindowsAzure.jpg',
  thumbnail: '/images/Thumbnail/WindowsAzure.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(azure);

var vagrant_virtualization = {
  url: "vagrant-virtualization",
  title: 'Vagrant & Virtualization',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Vagrant.jpg',
  thumbnail: '/images/Thumbnail/Vagrant.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(vagrant_virtualization);

var docker = {
  url: "docker",
  title: 'docker',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Docker.jpg',
  thumbnail: '/images/Thumbnail/Docker.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(docker);

var jenkins = {
  url: "jenkins",
  title: 'jenkins',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Jenkins.jpg',
  thumbnail: '/images/Thumbnail/Jenkins.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(jenkins);

var git = {
  url: "git",
  title: 'GIT',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Git.jpg',
  thumbnail: '/images/Thumbnail/Git.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

CoursesInfo.push(git);

var build_tools = {
  url: "build-tools",
  title: 'ANT,MAVEN,GRADLE',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: 'http://cdn.rancher.com/wp-content/uploads/2016/03/28193626/Container-Devops-Pipeline.png',
  thumbnail: '/images/Thumbnail/Maven.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

// CoursesInfo.push(build_tools);

var linux_admin = {
  url: "linux-admin",
  title: 'LINUX ADMINISTRATION',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Linux.jpg',
  thumbnail: '/images/Thumbnail/Redhat.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

// CoursesInfo.push(linux_admin);

var python= {
  url: "python",
  title: 'PYTHON',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Python.jpg',
  thumbnail: '/images/Thumbnail/Python.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

// CoursesInfo.push(python);

var ruby= {
  url: "ruby-rails",
  title: 'RUBY/RAILS',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Ruby.jpg',
  thumbnail: '/images/Thumbnail/Ruby.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

// CoursesInfo.push(ruby);


var shell= {
  url: "shell-scripting",
  title: 'SHELL SCRIPTING',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: 'http://cdn.rancher.com/wp-content/uploads/2016/03/28193626/Container-Devops-Pipeline.png',
  thumbnail: '/images/Thumbnail/Linux.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

// CoursesInfo.push(shell);


var perl= {
  url: "perl-scripting",
  title: 'PERL SCRIPTING',
  duration: '(45 hours - 45 days)',
  newBatch: 'Aug 21, 2016',
  price: '15,000',
  originalPrice: '20,000',
  courseHighlights: [
    "Starting from very basics to Advanced",
    "Laser focused realtime classroom trainings",
    "Placed ~400 students in different MNCs"
  ],
  headerImage: '/images/Banner/Perl.jpg',
  thumbnail: '/images/Thumbnail/Perl.jpg',
  upcomingClasses: [
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Classroom',
      start:"Oct 26, 2016",
      fee: "20,000"
    },
    {
      type: 'Online',
      start:"Nov 26, 2016",
      fee: "20,000"
    }
  ],
  courseContent: [
    {
      header: "1. Introduction to Build & Release Terminology",
      description:"<b>Learning Objectives -</b> In this module you will learn basics of Build and Release <br><b>Topics -</b> What is, Build, Deployment, Release, SCM. Compilation and why we need to compile the source code. Compilation languages Vs Interpreted languages, Source code Vs Object/Binary code, Java Packaging, Jar, War, EAR, General purpose packaging tools Zip, Tar, Tar.gz, .exe, .bin..etc. What is javac, jvm and other java tools"
    },
    {
      header: "2. Build and Release process for different types of software applications",
      description:"<b>Learning Objectives -</b>Web Applications, Standalone applications, packaging <br><b>Topics -</b> Working of DNS Server at Internet Scale, DNS Installation, DNS Configuration, DNS Tuning and Geolocation. Understand Web Servers like Apache, Ngnix and their differences, Configure Apache and Nginx for the Enterprise, Load Balancing through HA Proxy and Setup NFS for storage presentation."
    }
  ],
  certification: "GAMUTgurus Certification(logo) :\n\nAt the end of your course, you will work on a real time Project. You will receive a Problem Statement along with a data set to work.\n\nOnce you are successfully through with the project (reviewed by an expert), you will be awarded a certificate with a performance based grading.\n\nIf your project is not approved in 1st attempt, you can take additional assistance to understand the concepts better and reattempt the Project free of cost.",
  reimbersement: "You will get quotation and we can work with your company HR/Manager so that your reimbersement process go smooth.",
  trainer: "All trainers are realtime trainers having 12 years of experience. For DevOps you can put Nageswara Rao's profile. Highlight your name."
}

// CoursesInfo.push(perl);
