Template.info.onCreated(function(){
  let self = this;
  self.dataDict = new ReactiveDict();

  self.dataDict.set('selectedCourse', "All");

  Tracker.autorun(function () {
    Meteor.subscribe("testimonials", Session.get('testimonialLimit') || 50);

    self.testimonialHandle = Meteor.subscribe('testimonialCount');
  });
  
})

Template.info.helpers({
  buildAnReleaseInfo: function(){
    return Courses.findOne({ title: "BUILD AND RELEASE"});
  },
  devopsInfo: function(){
    return Courses.findOne({ title: "DEVOPS"});
  },
  linuxAdminInfo: function(){
    return Courses.findOne({ title: "DEVOPS"});
  },
  canShowMore: function(){
    let total = Counts.get("testimonialCount");
    let limit = Testimonials.find({}).count();

    return total > limit;
  },
  testimonials: function(){
    let res = Testimonials.find({}, { sort: { position: -1, positionUpdate: -1 } }).fetch();

    res.forEach(function (post) {
      if (!post.position) {
        post.position = 9999999;
      }
    });

    res = res.sort((a, b) => a.position - b.position );
    console.log(res);
    return res;

    let t = Template.instance();

    let selectedCourse = t.dataDict.get('selectedCourse');

    if( selectedCourse && selectedCourse !== "All"){
      return Testimonials.find({ course: selectedCourse}, { sort: { createdAt: -1 } })
    }
    return Testimonials.find({}, { sort: { createdAt: -1 } });
  },
  announcement: function(){
    var res = Announcement.findOne({});
    if(res && res.text){
      return res.text.replace(/\r\n|\r|\n/g,"<br />");
    }
  },
  blink: function(){
    blink();
    Meteor.setTimeout(function(){
      blink();
    }, 2000);
  },
  enableSlickDemo: function(){
    Session.set('rerun', Random.id());
  },
  courseSearchSettings: function(){
    return {
      position: "bottom",
      limit: 5,
      rules: [
        {
          token: '',
          collection: Courses,
          field: "title",
          template: Template.matchCourse,
          matchAll: true,
          sort: true,
          noMatchTemplate: Template.noMatchCourse,
          selector: function(match) {
            var regex = new RegExp(match, 'i')
            return {$or: [{'title': regex}, {'aliases': regex}]}
          }
            
        }
      ]
    };
  },
  rating() {
    const res = Testimonials.find({}).fetch();
    console.log(res);

    const total = res.length;

    const allReviews = res.reduce((a, b) => a.rating + b.rating);

    const avg = allReviews / total;

    if (!avg || avg < 4.8) {
      return 4.8
    }
    return avg;
  },
  totalReviewsCount() {
    return Testimonials.find({}).count();
  }
});

Template.info.onRendered(function(){
  
  // Tracker.autorun(function () {
  //   Session.get('rerun');
  // setSlickDemo();
  // });
  const self = this;
  
  Tracker.autorun(function () {
    if (self.testimonialHandle.ready()) {
      Meteor.defer(() => {
        setSlickDemo();
        $('.profile').initial();
      })
    }
    
  });
});

Template.info.events({
  "autocompleteselect input": function(event, template, doc) {
    FlowRouter.go('/course/'+doc.url);
  },
  'change #selectCourse': function (e, t) {
    t.dataDict.set('selectedCourse', e.currentTarget.value)
  },
  'click #loadMore': function(e, t) {
    let limit = Session.get('testimonialLimit') || 50;
    Session.set('testimonialLimit', limit + 50);
  }
});


Template.chatBox.events({
  'click .open-small-chat': function () {
    $('.small-chat-icon').toggleClass('fa-comments').toggleClass('fa-remove');
    $('.small-chat-box').toggleClass('active');
  },
  'click #closeChatBox': function(){
    $(".open-small-chat").trigger('click');
  },
  'click #sendEmail': function(){
    toastr.clear();
    var fullName = $("#fullName").val();
    if(!fullName || !fullName.trim()){
      toastr.error("Full name cannot be empty");
      return;
    }

    var email = $("#email").val();
    if(!email || !validEmail(email)){
      toastr.error("Please enter your valid email address");
      return;
    }

    var queryCourse = $("#queryCourse").val();
    if(!queryCourse || queryCourse === "Select Course"){
      toastr.error("Please select course");
      return;
    }

    var mobileNumber = $("#mobileNumber").val();
    
    if(!mobileNumber || !validatePhoneNumber(mobileNumber) ){
      toastr.error("Please enter 10 digit mobile number");
      return;
    }

    var message = $("#message").val();
    if(!message){
      toastr.error("Please enter your message");
      return;
    }
    Meteor.call('sendEmail', fullName, email, mobileNumber, message, queryCourse, function (error, result) {
      if(error){
        toastr.error(error.reason);
      }else{
        toastr.success("Message sent.");
        $("#fullName").val("");
        $("#email").val("");
        $("#mobileNumber").val("");
        $("#message").val("");
        $(".open-small-chat").trigger('click')
      }
    });
  }
});

validEmail = function(email){
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}






blink = function () {

  Meteor.setTimeout(function(){

    var colors = [ 'coral', 'white','aqua', 'aquamarine', 'yellow', 'chartreuse', 'darkkhaki', 'thistle',
  "sandybrown", "powderblue", "plum", "palevioletred", "palegoldenrod", "orange"];
    var newColor = _.shuffle(colors)[0];
    $('.blink_me').css('color', newColor);

  }, 800);
  $('.blink_me').fadeOut(800).fadeIn(800, blink);
};


setSlickDemo = function(){
  // $('.testimonials-div').slick('unslick');
  $('.testimonials-div').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    rows: 2,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  // $('.slick_demo_2').slick({
  //   infinite: true,
  //   slidesToShow: 5,
  //   slidesToScroll: 1,
  //   centerMode: true,
  //   responsive: [
  //     {
  //       breakpoint: 1024,
  //       settings: {
  //         slidesToShow: 5,
  //         slidesToScroll: 5,
  //         infinite: true,
  //         dots: false
  //       }
  //     },
  //     {
  //       breakpoint: 600,
  //       settings: {
  //         slidesToShow: 2,
  //         slidesToScroll: 2
  //       }
  //     },
  //     {
  //       breakpoint: 480,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1
  //       }
  //     }
  //   ]
  // });
}

Template.DevOpsCourses.helpers({
  DevOpsCourses: function () {
    return [
      {
        name: "Chef",
        logo: "/images/Thumbnail/Chef.jpg",
        url: 'chef'
      },
      {
        name: "Puppet",
        logo: "/images/Thumbnail/Puppet.jpg",
        url: 'puppet'
      },
      {
        name: "Ansible",
        logo: "/images/Thumbnail/Ansible.jpg",
        url: "ansible"
      },
      {
        name: "AWS",
        logo: "/images/Thumbnail/Amazon.jpg",
        url: "aws"
      },
      {
        name: "Azure",
        logo: "/images/Thumbnail/WindowsAzure.jpg",
        url: "azure"
      },
      // {
      //   name: "Vagrant/VMWare/VirtualBox",
      //   logo: "/images/Thumbnail/VmWare.jpg",
      //   url: "vagrant-virtualization"
      // },
      {
        name: "Docker",
        logo: "/images/Thumbnail/Docker.jpg",
        url: 'docker'
      },
      {
        name: "Jenkins",
        logo: "/images/Thumbnail/Jenkins.jpg",
        url: 'jenkins'
      },
      {
        name: "GIT",
        logo: "/images/Thumbnail/Git.jpg",
        url: 'version-control'
      },
      // {
      //   name: "ANT",
      //   logo: "/images/Thumbnail/Apache.jpg",
      //   url: 'build-tools'
      // },
      {
        name: "MAVEN",
        logo: "/images/Thumbnail/Maven.jpg",
        url: 'build-tools'
      },
      {
        name: "GRADLE",
        logo: "/images/Thumbnail/Gradle.jpg",
        url: 'build-tools'
      },
    ]
  }
});

Template.buildAndReleaseCourse.helpers({
  buildAndReleaseCourse: function () {
    return [
      {
        name: "Python",
        logo: "/images/Thumbnail/Python.jpg"
      },
      {
        name: "Ruby/Rails",
        logo: "/images/Thumbnail/Ruby.jpg"
      },
      {
        name: "Shell",
        logo: "/images/Thumbnail/Linux.jpg"
      },
      {
        name: "PERL",
        logo: "/images/Thumbnail/Perl.jpg"
      }
    ]
  }
});

Template.scriptingCourse.helpers({
  scriptingCourse: function () {
    return [
      {
        name: "ANT/MAVEN/Graddle",
        logo: "/images/Thumbnail/Maven.jpg"
      },
      {
        name: "GIT",
        logo: "/images/Thumbnail/Git.jpg"
      },
      {
        name: "Jenkins",
        logo: "/images/Thumbnail/Jenkins.jpg"
      }
    ]
  }
});

Template.linuxCourse.helpers({
  linuxCourse: function () {
    return [
     {
        name: "Linux Admin",
        logo: "/images/Thumbnail/Redhat.jpg"
      },
    ]
  }
});

Template.noMatchCourse.events({
  'click #NewCourse': function () {
    $('.small-chat-icon').removeClass('fa-comments').addClass('fa-remove');
    $('.small-chat-box').addClass('active');

    let user = Meteor.user();

    $("#message").val("Looking for new course: "+$("#courseSearch").val())

    if( user){
      $("#fullName").val(user.profile.name);
      $("#mobileNumber").val(user.profile.mobile);
      $("#email").val(user.emails[0].address);
    }
  }
});
