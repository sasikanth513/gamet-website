Template.Trainers.onCreated(function(){
  var self = this;
  self.dataDict = new ReactiveDict();
});

Template.Trainers.helpers({
  personalImages() {
    return [
      'https://farm5.static.flickr.com/4204/35420353965_b21436c14e_s.jpg',
      'https://farm5.static.flickr.com/4209/34581259844_6546056b74_b.jpg',
      'https://farm5.static.flickr.com/4231/35415013105_e188966720_b.jpg',
    ];
  },
  images() {
    return [
      'https://farm5.static.flickr.com/4204/35420353965_b21436c14e_s.jpg',
      'https://farm5.static.flickr.com/4209/34581259844_6546056b74_b.jpg',
      'https://farm5.static.flickr.com/4231/35415013105_e188966720_b.jpg',
      'https://farm5.static.flickr.com/4236/35032718700_3d5a33e421_b.jpg',
      'https://farm5.static.flickr.com/4275/35035419660_93b3db73b7_s.jpg',
      'https://farm5.static.flickr.com/4198/35257330332_4c1ff8d079_b.jpg'
    ];
  }
});

Template.Trainers.onRendered(function(){

})

Template.Trainers.events({
  'submit #trainerForm': function(e, t){
    e.preventDefault();

    let obj = {};

    obj.firstName = t.$("#firstName").val();

    if( !obj.firstName || !obj.firstName.trim() ){
      toastr.error("First Name cannot be empty.");
      return;
    }

    obj.lastName = t.$("#lastName").val();

    if( !obj.lastName || !obj.lastName.trim() ){
      toastr.error("Last Name cannot be empty.");
      return;
    }

    obj.mobileNumber = t.$("#mobileNumber").val();

    if( !obj.mobileNumber || !obj.mobileNumber.trim() ){
      toastr.error("Mobile Number cannot be empty.");
      return;
    }

    obj.email = t.$("#email").val();

    if( !obj.email || !obj.email.trim() ){
      toastr.error("Email cannot be empty.");
      return;
    }


    obj.courseName = t.$("#courseName").val();

    if( !obj.courseName || !obj.courseName.trim() ){
      toastr.error("Course Name cannot be empty.");
      return;
    }


    obj.aboutCourse = t.$("#aboutCourse").val();

    if( !obj.aboutCourse || !obj.aboutCourse.trim() ){
      toastr.error("About Course cannot be empty.");
      return;
    }

    obj.aboutYourSelf = t.$("#aboutYourSelf").val();

    if( !obj.aboutYourSelf || !obj.aboutYourSelf.trim() ){
      toastr.error("About Yourself cannot be empty.");
      return;
    }

    Meteor.call('sendTrainerEmail', obj, function (error, result) {
      if(error){
        toastr.error(error.reason);
      }else{
        toastr.success("Message received. We will get back to you As soon as possible.");
        t.$("#firstName").val("");
        t.$("#lastName").val("");
        t.$("#mobileNumber").val("");
        t.$("#email").val("");
        t.$("#courseName").val("");
        t.$("#aboutCourse").val("");
        t.$("#aboutYourSelf").val("");
      }
    });
    console.log(obj);
  }
});

Template.Trainers.onDestroyed(function(){

});