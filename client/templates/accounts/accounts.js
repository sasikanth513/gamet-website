Template.Login.onCreated(function(){
  var self = this;
  self.dataDict = new ReactiveDict();
});

Template.Login.helpers({

});

Template.Login.onRendered(function(){
   $('#login-form-link').click(function(e) {
    $("#login-form").delay(100).fadeIn(100);
    $("#register-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
  });
  $('#register-form-link').click(function(e) {
    $("#register-form").delay(100).fadeIn(100);
    $("#login-form").fadeOut(100);
    $('#login-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
  });
})

Template.Login.events({
  'submit #register-form': function(e, t){
    e.preventDefault();

    var name = t.$("#userRegisteringName").val();
    if(!name || name.length < 6){
      toastr.error("Name should be atleast 6 characters.");
      return;
    }

    var email = t.$("#registerEmail").val();
    if(!email || !validateEmail(email)){
      toastr.error("Invalid e-mail.");
      return;
    }

    var mobile = t.$("#registerMobileNo").val();
    var res = validatePhoneNumber(mobile);
    if(!res){
      toastr.error("Please enter 10 digit mobile number");
      return;
    }

    var pwd = t.$("#registerPassword").val();
    var pwd1 = $("#confirm-password").val();

    if(pwd.length <6){
      toastr.error("Password must be atleast 6 characters.");
      return;
    }

    if(pwd !== pwd1){
     toastr.error("Passwords doesn't match.");
      return;
    }
    Accounts.createUser({
      password: pwd,
      email: email,
      profile: {
        name: name,
        mobile:mobile
      }
    }, function (error) {
      if(error){
        toastr.error(error.reason);
      }else{
        toastr.success("Account created. Please verify your email");
        FlowRouter.go("/")
      }
    });
  },
  'submit #login-form': function(e, t){
    e.preventDefault();
    var email = $("#userFullName").val();
    var password = $("#password").val();

    var allGood = true;
    if(!email){
      toastr.error("Please enter your email");
      allGood = false;
    }
    if(!password){
      toastr.error("Please enter password");
      allGood = false;
    }

    if(allGood){
      Meteor.loginWithPassword(email, password, function(err){
        if(err){
          toastr.error(err.reason);
        }else{
          FlowRouter.go("/")
        }
      })
    }
  },
  'click #forgotPassword': function(e, t){
    $("#pwdModal").modal('show');
  },
  'click #sendResetEmail': function(){
    let email = $("#resetEmail").val();
    if(!email || !validEmail(email)){
      toastr.error("Please enter your valid email address");
      return;
    }
    Meteor.call('sendResetEmail', email, function (error, result) {
      if(error){
        toastr.error(error.reason);
      }else{
        $("#pwdModal").modal('hide');
        toastr.success("Email sent");
      }
    });
  }
});

Template.Login.onDestroyed(function(){

});

validatePhoneNumber = function(value){
  return value.match(/\d/g).length===10;
}

Template.resetPassword.events({
  'click #resetPassword, submit #resetPasswordForm': function(e, t){
    e.preventDefault();
    var p = $("#password1").val();
    var p1 = $("#password2").val();

    if(p){
      p = p.trim();
      p1 = p1.trim();
    }
    if(!p){
      toastr.error("Please enter password");
      return;
    }

    if( p !== p1){
      toastr.error("Passowrd doesn't match");
      return;
    }
    if(p.length < 6){
      toastr.error('Password must be at least 6 characters');
      return;
    }
    var token = FlowRouter.current().params.token;
    Accounts.resetPassword(token, p,function(error){
      if(error){
        toastr.error(error.reason)
      }else{
        toastr.success("Passowrd reset");
        FlowRouter.go("/");
      }
    })
  }
});
