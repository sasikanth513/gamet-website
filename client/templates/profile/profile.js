Template.profile.onCreated(function(){
  let self = this;
  self.dataDict = new ReactiveDict();
  self.dataDict.set('queriesCourse', 'All');
  self.dataDict.set('selectedQueries', []);

  Tracker.autorun(function () {
    if(Meteor.user()){
      var emails = [ "sasi.kanth80@gmail.com", "nageshvkn@gmail.com" ];
      var email = Meteor.user().emails[0].address;
      if( emails.indexOf(email) > -1 ){
        Meteor.subscribe('adminQueries');
        console.log('subscribing to admin testimonials');
        Meteor.subscribe('adminTestimonials');
      }
    }  
  });
  
  
})
Template.profile.onRendered(function(){
  Tracker.autorun(function () {
    let testimonial = Testimonials.findOne({ userId: Meteor.userId() });
    // if( testimonial && testimonial.course){
    //   Meteor.setTimeout(function () {
    //     $("#testimonialCourse").val(testimonial.course)  
    //   }, 2000);
      
    // }
    const obj = {
      starWidth: '20px',
      fullStar: true
    };

    if (testimonial && testimonial.rating) {
      obj.rating = testimonial.rating;
    }
    // console.log('setting rate you ', obj);
    Meteor.setTimeout(function () {
      if (this.rateyo) {
        this.rateyo.rateYo("destroy");
      }
      this.rateyo =$("#rateYo").rateYo(obj);
    }, 1000);
  });
  
})
Template.profile.helpers({
  userBase: function(){
    return Meteor.users.find({}, { sort: { createdAt: -1 } });
  },
  queries: function(){
    let t = Template.instance();
    let course = t.dataDict.get('queriesCourse');
    if( course && course !== "All"){
      return Queries.find({ course: course }, { sort: { createdAt: -1 } })
    }
    return Queries.find({}, { sort: { createdAt: -1 } });
  },
  testimonial: function(){
    var testimonial = Testimonials.findOne({ userId: Meteor.userId() });
    if(testimonial && testimonial.text){
      return testimonial.text;
    }
    return false;
  },
  admin: function(){
    if(Meteor.user()){
      var emails = [ "sasi.kanth80@gmail.com", "nageshvkn@gmail.com" ];
      var email = Meteor.user().emails[0].address;
      return emails.indexOf(email) > -1;
    }
  },
  announcement: function(){
    var res = Announcement.findOne({});
    if(res && res.text){
      return res.text;
    }
  },
  allTestimonials() {
    let res = Testimonials.find({}, { sort: { position: -1, positionUpdate: -1 } }).fetch();

    res.forEach(function (post) {
      if (!post.position) {
        post.position = 9999999;
      }
    });

    res = res.sort((a, b) => a.position - b.position );
    return res;
  },
  parseDate(d) {
    return new Date(d).toLocaleString()
  }
});

Template.profile.events({
  'click #moveToGamutians': function(e, t){

  },
  'change .i-checks': function(e, t){
    let self = this;

    let selectedQueries = t.dataDict.get('selectedQueries') || [];

    if( e.currentTarget.checked ){
      if( selectedQueries.indexOf(self._id) < 0 ){
        selectedQueries.push(self._id);
      }
    }else{
      if( selectedQueries.indexOf(self._id) > -1 ){
        selectedQueries.splice(selectedQueries.indexOf(self._id), 1);
      }
    }

    t.dataDict.set('selectedQueries', selectedQueries)
  },
  'change #queriesCourse': function(e, t){
    t.dataDict.set('queriesCourse', $("#queriesCourse").val());
  },
  "click #showActionModalToggle": function(){
    $("#showActionModal").modal('show');
  },
  "click #cancelTestimonial": function(event, template){
     event.preventDefault();
     $("#testimonial").val("");
  },
  "click #saveTestimonial": function(event, template){
     event.preventDefault();
     var text = $("#testimonial").val();
     if(!text || text.length < 30){
       toastr.error("Testimonial should be atleast 30 characters length.");
       return;
     }

     // let testimonialCourse = $("#testimonialCourse").val();

     // if( !testimonialCourse || testimonialCourse === "Select Course"){
     //    toastr.error("Please select the course you're writing testimonial for.");
     //    return;
     // }
     const rating = $("#rateYo").rateYo("rating");
     if( !rating){
        toastr.error("Please select your rating.");
        return;
     }

     Meteor.call("saveTestimonial", text, "DevOps", rating, function(error, result){
       if(error){
         toastr.error("Something went wrong. Please try agian later.")
       }
       if(result){
         toastr.success("Testimonial submitted.");
         $("#testimonial").val("");
       }
     });
  },
  "click #updateTestimonial": function(event, template){
     event.preventDefault();
     var text = $("#testimonialEdit").val();
     if(!text || text.length < 30){
       toastr.error("Testimonial should be atleast 30 characters length.");
       return;
     }
     // let testimonialCourse = $("#testimonialCourse").val();

     // if( !testimonialCourse || testimonialCourse === "Select Course"){
     //    toastr.error("Please select the course you're writing testimonial for.");
     //    return;
     // }

     const rating = $("#rateYo").rateYo("rating");
     if( !rating){
        toastr.error("Please select your rating.");
        return;
     }

     Meteor.call("updateTestimonial", text, "DevOps", rating, function(error, result){
       if(error){
         toastr.error("Something went wrong. Please try agian later.")
       }
       if(result){
         toastr.success("Testimonial updated.");
         $("#testimonialEdit").val(text);
       }
     });
  },
  'click #logout': function(){
    Meteor.logout(function(){
      toastr.success("Logged out.");
      FlowRouter.go("/");
    });
  },
  'click #updatePwd': function(e, t){
    e.preventDefault();

    var oldpwd = $("#oldPassword").val();
    var newpwd = $("#newPassword").val();
    var newpwd1 = $("#newPassword1").val();

    if(!oldpwd || !newpwd || !newpwd1){
      toastr.error("All fields are mandatory.");
      return;
    }

    if(newpwd.length < 6){
      toastr.error("Password should be atleast 6 characters.");
      return;
    }

    if(oldpwd === newpwd){
      toastr.error("Old password and new password are same.");
      return;
    }
    Accounts.changePassword(oldpwd, newpwd, function(err){
      if(err){
        toastr.error(err.reason);
      }else{
        toastr.success("Password chagned.");
        $("#oldPassword").val("");
        $("#newPassword").val("");
        $("#newPassword1").val("");
      }
    });
  },
  'click #saveSpecialAnnouncement': function(e, t){
    e.preventDefault();
    var text = $("#announcement").val();

    Meteor.call("saveAnnouncement", text, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
         toastr.success("Announcement saved");
      }
    });
  },
  'click #saveActions': function(e, t){
    let res = confirm("Are you sure you want changes? Need to display fees course and names here.");

    if( !res ){
      return;
    }

    let queries = t.dataDict.get('selectedQueries');

    if( queries.length === 0 ){
      toastr.error("Please select at least one query.");
      return;
    }

    let courses = [];

    if( $("#DevOps").is(":checked") ){
      courses.push("DevOps")  
    }

    if( $("#AWS").is(":checked") ){
      courses.push("AWS")  
    }

    if( $("#BuildandRelease").is(":checked") ){
      courses.push("Build and Release")  
    }

    if( $("#Course1").is(":checked") ){
      courses.push("Course 1")  
    }

    if( $("#Course2").is(":checked") ){
      courses.push("Course 2")  
    }

    let fees = $("#actionFees").val();
    let remarks = $("#actionRemarks").val();
    
    Meteor.call('updateQueries', queries, courses, fees, remarks, function (error, result) {
      if(error){
        toastr.error(error.reason);
      }else{
        toastr.success("Updated");
        $("#showActionModal").modal('hide');
      }
    });
  },
  'click #updateTestRecord': function (e, t) {
    const approved = $('#approved_'+this._id).is(':checked');
    const position = $('#position_'+this._id).val();
    if (typeof approved !== 'undefined' && typeof position !== 'undefined') {
      Meteor.call('updateTestAdmin', approved, position, this._id, () => {})
    }
  }
});
