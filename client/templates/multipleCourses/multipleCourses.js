Template.multipleCourses.onCreated(function(){
  var self = this;
  self.dataDict = new ReactiveDict();
});

Template.multipleCourses.helpers({
  buildAndReleaseCourse: function () {
    return [
      {
        name: "Linux Admin",
        logo: "/images/Thumbnail/Redhat.jpg"
      },
      {
        name: "Python",
        logo: "/images/Thumbnail/Python.jpg"
      },
      {
        name: "Ruby/Rails",
        logo: "/images/Thumbnail/Ruby.jpg"
      },
      {
        name: "Shell",
        logo: "/images/Thumbnail/Linux.jpg"
      },
      {
        name: "PERL",
        logo: "/images/Thumbnail/Perl.jpg"
      }
    ]
  }
});

Template.multipleCourses.onRendered(function(){

})

Template.multipleCourses.events({

});

Template.multipleCourses.onDestroyed(function(){

});