Template.navbar.onRendered(function(){
  
  Tracker.autorun(function () {
    Meteor.user();
    Meteor.setTimeout(function () {
      
      $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
      });  

    }, 1000);
    
  });
  

  Tracker.autorun(function () {
    var name = FlowRouter.getRouteName();
    if(name !== "home"){
      $('.navbar').addClass('navbar-scroll');
    }else{

      $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 80
      });

      // Page scrolling feature
      $('a.page-scroll').bind('click', function(event) {
          var link = $(this);
          $('html, body').stop().animate({
              scrollTop: $(link.attr('href')).offset().top - 50
          }, 500);
          event.preventDefault();
          $("#navbar").collapse('hide');
      });

      var cbpAnimatedHeader = (function() {
        var docElem = document.documentElement,
                header = document.querySelector( '.navbar-default' ),
                didScroll = false,
                changeHeaderOn = 200;
        function init() {
            window.addEventListener( 'scroll', function( event ) {
                if( !didScroll ) {
                    didScroll = true;
                    setTimeout( scrollPage, 250 );
                }
            }, false );
        }
        function scrollPage() {
          name = FlowRouter.getRouteName();
          var sy = scrollY();
          if ( sy >= changeHeaderOn ) {
            $(header).addClass('navbar-scroll');
          }
          else if(name === "home"){
            $(header).removeClass('navbar-scroll')
          }
          didScroll = false;

          // var name = FlowRouter.getRouteName();
          // if(name !== "home"){
          //   $('.navbar').addClass('navbar-scroll');
          // }
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        init();

        // var name = FlowRouter.getRouteName();
        // if(name !== "home"){
        //   $('.navbar').addClass('navbar-scroll');
        // }

      })();

      // Activate WOW.js plugin for animation on scrol
      new WOW().init();

    }
  });


  // $(".open-small-chat").trigger('click');
})


Template.navbar.events({
  "click #goHome": function(event, template){
     FlowRouter.go("/");
  },
  "click #goHome": function(event, template){
     FlowRouter.go("/");
  },
  'click #goToCourses': function(e, t){
    FlowRouter.go("/courses");
  },
  'click #goToTestimonials': function(e, t) {
    FlowRouter.go("/testimonials");
  },
  'click #logout': function(){
    Meteor.logout(function(){
      toastr.success("Logged out.");
      FlowRouter.go("/");
    });
  }
});
