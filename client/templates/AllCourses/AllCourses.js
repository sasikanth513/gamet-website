Template.AllCourses.onCreated(function(){
  var self = this;
  self.dataDict = new ReactiveDict();
});

Template.AllCourses.helpers({
  AllCourses: function(){
    return CoursesInfo;
  },
  description: function(){
    return this.courseContent[0].description.slice(0, 150)
  },
  courseSearchSettings: function(){
    return {
      position: "bottom",
      limit: 5,
      rules: [
        {
          token: '',
          collection: Courses,
          field: "title",
          template: Template.matchCourse,
          matchAll: true,
          sort: true,
          noMatchTemplate: Template.noMatchCourse
        }
      ]
    };
  },
});

Template.AllCourses.onRendered(function(){

})

Template.AllCourses.events({
  "autocompleteselect input": function(event, template, doc) {
    FlowRouter.go('/course/'+doc.url);
  },
});

Template.AllCourses.onDestroyed(function(){

});