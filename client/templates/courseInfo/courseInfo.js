Template.courseInfo.onCreated(function(){
  var self = this;
  self.dataDict = new ReactiveDict();

  Meteor.subscribe("testimonials", 100);
});

Template.courseInfo.helpers({
  courseDetails: function(){
    let slug = FlowRouter.current().params.slug;

    let res = CoursesInfo.find(function(t){ return t.url === slug });
    return res;
  },
  testimonials: function(){
    return Testimonials.find({}, { sort: { createdAt: -1 } });
  }
});

Template.courseInfo.onRendered(function(){

})

Template.courseInfo.events({
  'click #enrollNow': function(e, t){
    let self = this;
    let user = Meteor.user();
    
    $('.small-chat-icon').removeClass('fa-comments').addClass('fa-remove');
    $('.small-chat-box').addClass('active');

    if( user){
      $("#fullName").val(user.profile.name);
      $("#mobileNumber").val(user.profile.mobile);
      $("#email").val(user.emails[0].address);
    }
    let slug = FlowRouter.current().params.slug;

    let course = CoursesInfo.find(function(t){ return t.url === slug });

    if( course ){
      let msg = "Dear Sir.\n I'm interested in following course.\n title: "+ course.title+"\n type: "+self.type+"\n Starting Date: "+self.start+"\n Fee: "+self.fee;

      $("#message").val(msg);

    }

    
    
  }
});

Template.courseInfo.onDestroyed(function(){

});